package com.sineatos.kafkatesting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SimpleConsumer {

    @KafkaListener(topics = "test.topic")
    public void listen(@Payload String message) {
        log.info("Consumer get message: " + message);
    }

}
