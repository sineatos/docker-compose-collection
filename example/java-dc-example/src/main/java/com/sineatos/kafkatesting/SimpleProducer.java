package com.sineatos.kafkatesting;

import com.sineatos.config.KafkaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class SimpleProducer {

    @Autowired
    private KafkaConfig kafkaConfig;

    @Autowired
    private KafkaTemplate<String, String> template;

    public void send(String message) {
        template.send(kafkaConfig.getTopic(), message);
    }

}
