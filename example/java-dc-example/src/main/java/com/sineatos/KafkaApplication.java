package com.sineatos;

import com.sineatos.kafkatesting.SimpleProducer;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.kafka.annotation.EnableKafka;

import java.util.Date;
import java.util.Scanner;

@Slf4j
@EnableKafka
@SpringBootApplication
public class KafkaApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(KafkaApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
        SimpleProducer producer = context.getBean(SimpleProducer.class);
        for (int i = 0; i < 10; i++) {
            log.info("producer send Hello World, {} time", i);
            producer.send("Hello World " + i + " @" + new Date());
        }
        Scanner input = new Scanner(System.in);
        while (input.hasNext()) {
            String string = input.nextLine();
            if ("stop".equals(string)) {
                break;
            }
            producer.send("send message " + string + " @" + new Date());
        }
        context.close();
        System.exit(0);
    }

}
