# docker-compose-collection

## 介绍

常用的docker compose配置集合，方便使用docker compose快速创建环境

- 需要配合docker-compose使用
- 安装docker-compose：上Github下载或者随docker一起安装

## 项目结构

```
|-docker-compose-collection
    |-zk-cluster                # 每一个docker-compose配置及相关配置所在目录，使用时只需复制整个目录并修改
        |-conf                  # 配置目录，里面会放置运行实例用到的各种配置
        |-storage               # 存放数据的目录，这些数据通常是运行实例产生的，不是标品的一部分，会被git排除
        |-其它目录
```

## 目录说明

- `zk-cluster`: 快速创建zookeeper集群
    - 使用脚本`zk-config-gen.py`能快速生成指定规模集群节点的docker-compose模板和zoo.cfg
    - 包含五个节点集群的docker-compose.yml和zoo.cfg样例
- `nginx`: 创建一个显式静态页面的nginx实例
    - 静态页面存放地址：`./storage/www`
    - 日志存放地址：`./storage/logs`
    - 默认会将nginx的端口映射为宿主机的`8080`，根据需要调整
    - 环境变量根据需要调整
- `kafka`：创建一个zookeeper、kafka和kafka-manager实例
    - zookeeper配置为`./conf/zoo.cfg`